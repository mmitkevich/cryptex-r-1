library(cryptexr)
options(pillar.sigfig=7, digits.secs=4)  

snapshot_info <- function(data) {
  d = ifnull(data$op, data, data %>% filter(op=='snapshot') %>% group_by(client_id) %>% nest())
  d$bid = d$data %>% map_dbl(~ bids(.)[1,]$price)
  d$bid_qty = d$data %>% map_dbl(~ bids(.)[1,]$qty)
  d$bid_ts = d$data %>% map_dbl(~ bids(.)[1,]$client_ts)  %>% as_datetime()
  d$ask = d$data %>% map_dbl(~ asks(.)[1,]$price)
  d$ask_qty = d$data %>% map_dbl(~ asks(.)[1,]$qty)
  d$ask_ts = d$data %>% map_dbl(~ asks(.)[1,]$client_ts) %>% as_datetime()
  d$client_ts = ifnull(d$client_ts, d$data %>% map_dbl( ~ max(.$client_ts)) %>% as_datetime(), d$client_ts %>% as_datetime())
  return(d)
}

convert_l2 <- function(data) {
  r = data %>% level2_to_level1(list(index=1))
  
  cat("out:\n"); print(r$out)
  cat("index=", r$index, "\n")
  cat("bids:\n"); print(bids(r$snapshot))
  cat("asks:\n"); print(asks(r$snapshot))
  
  cat("calculated:\n"); snapshot_info(r$calculated)
  cat("received:\n"); snapshot_info(r$received)
  cat("data:\n"); snapshot_info(data)
  r
}

tdb_connect(host="lulzex.com", user="covexity", password="rmustdie")

symbols <- c(
#"BTC:USD@HITBTC_GATEWAY",
#"BTC:THIS_WEEK@OKEX_GATEWAY",
"BTC:USD@BITMEX_GATEWAY"
)

data = tdb_query(from="order_book", symbols=symbols, start_ts = now()-hours(6))
r = data %>% convert_l2()

seq_along(r$calculated$data) %>% 
  map(~ 
      plot_lob(
          list(calc=r$calculated$data[[.]], 
                 recv=r$received$data[[.]]), 
                 title=paste0(
                        r$calculated$client_ts[[.]] %>% as.character(),
                        " ",
                        symbols)))
