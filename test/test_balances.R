library(cryptexr)

tdb_connect()

bals <- tdb_query(from="balances", 
                  start_ts = now()-days(5)) %>% 
  mutate(price=BTC) %>% tdb_to_freq(hours(1))

tdb_plot.balances(bals)
