#include "rutils.h"
#include <string.h>
#include <cmath>

using namespace Rcpp;

struct Order {
  double price;
  double qty;
  
  Order(double price=NAN, double qty=0.) : 
    price(price), qty(qty) {}
  
  int side() { return sgn(qty); }
};

struct OrderMessage: Order {
  long flags;
  
  double ts;
  long server_id;
  long client_id;

  enum {
    SNAPSHOT = 1,
    UPDATE = 2,
    INSERT = 4,
    REMOVE = 8,
    TRADE = 16,
    BID = 32,
    ASK = 64,
    BEST = 128
  };
  
  explicit OrderMessage(double price=NAN, double qty=0., double ts=NAN, long flags=UPDATE, long client_id=0, long server_id=0) : 
    Order(price, qty), flags(flags), ts(ts) {}
  
  static OrderMessage from(const Order &ord, double ts, long flags=UPDATE) {
    return OrderMessage(ord.price, ord.qty, ts, flags);
  }
};

class DoubleLess : public std::binary_function<double,double,bool>
{
public:
  DoubleLess( double arg_ = 1e-6 ) : epsilon(arg_) {}
  bool operator()( const double &left, const double &right  ) const
  {
    // you can choose other way to make decision
    // (The original version is: return left < right;) 
    return (left - right < -epsilon);
  }
  double epsilon;
};

typedef std::map<double, OrderMessage, DoubleLess> PriceOrderMap;

struct Book : PriceOrderMap {
  iterator bid_itr = end();
  iterator ask_itr = end();
  double ts;
  
  typedef PriceOrderMap base_type;
  enum {
    BID = 1,
    ASK = 2
  };
  
  Book() : ts(NAN) { }

  const OrderMessage bid() {
    return bid_itr->second;
  }
  
  const OrderMessage ask() {
    return ask_itr->second;
  }
  
  
  const OrderMessage at(double price) {
    auto level_itr = find(price);
    if(level_itr==end())
      return OrderMessage(price);
    return level_itr->second;
  }
  
  int update(const OrderMessage &order) {
    ts = order.ts;
    
    
    int flags = 0;
    auto level_itr = find(order.price);
    if(level_itr == end())
      level_itr = insert(Book::value_type(order.price, order)).first;
    level_itr->second = order;
    
    if(order.qty>0 && (bid_itr==end() || bid_itr->first<order.price)) {
      bid_itr = level_itr;
      flags |= BID;
    }
    if(order.qty<0 && (ask_itr==end() || ask_itr->first>order.price)) {
      ask_itr = level_itr;
      flags |= ASK;
    }
    
    if(is_zero(level_itr->second.qty)) {
      if(bid_itr==level_itr) {
        bid_itr++;
        flags |= BID;
      }
      if(ask_itr==level_itr) {
        if(ask_itr!=begin())
          ask_itr--;
        else
          ask_itr = end();
        flags |= ASK;
      }
      erase(level_itr);
    }
    return flags;
  }

  void clear() {
    base_type::clear();
    bid_itr = end();
    ask_itr = end();
  }

  List toR() const {
    List result;
    NumericVector price(size());
    NumericVector qty(size());
    NumericVector ts(size());
    size_t i=0;
    for(auto p: *this) {
      price[i] = p.first;
      qty[i] = p.second.qty;
      ts[i] = p.second.ts;
      i++;
    }
    result.push_back(price, "price");
    result.push_back(qty, "qty");
    result.push_back(Function("as_datetime")(ts), "client_ts");
    return Function("as_tibble")(result);
  }
};

const char* format_op(const OrderMessage &op) {
  if(op.flags & OrderMessage::BID)
    return "bid";
  if(op.flags & OrderMessage::ASK)
    return "ask";
  if(op.flags & OrderMessage::SNAPSHOT)
    return "snapshot";
  if(op.flags & OrderMessage::UPDATE)
    return "update";
  if(op.flags & OrderMessage::TRADE)
    return "trade";
  return "<unknown>";
}

long parse_op(const char* op) {
  if(!strcmp(op, "snapshot"))
    return OrderMessage::SNAPSHOT;
  if(!strcmp(op, "update"))
    return OrderMessage::UPDATE;
  if(!strcmp(op, "trade"))
    return OrderMessage::TRADE;
  return 0;
}

List toR(const std::vector<Book> &books) {
  List result;
  List bs;
  NumericVector ts(books.size());
  for(int i=0; i<books.size(); i++) {
    bs.push_back(books[i].toR());
    ts[i] = books[i].ts;
  }
  result.push_back(Function("as_datetime")(ts), "client_ts");
  result.push_back(bs, "data");
  return Function("as_data_frame")(result);
}

//' level2_to_level1
//'
//' will transform level2 data to level1
//' @export
// [[Rcpp::export]]
List level2_to_level1(List data, List params) {
  struct Input {
    NumericVector ts;
    NumericVector price;
    NumericVector qty;
    CharacterVector op;
    NumericVector client_id;
    NumericVector server_id;
    
    Input(List data) :
      ts(required<NumericVector>(data, "client_ts")),
      price(required<NumericVector>(data, "price")),
      qty(required<NumericVector>(data, "qty")),
      op(optional<CharacterVector>(data, "op")),
      client_id(optional<NumericVector>(data, "client_id")),
      server_id(optional<NumericVector>(data, "server_id"))
    {}
    
    int nrows() const {
      return ts.length();
    }
    
    bool find_first(size_t &start, std::function<bool(const Input &inp, int i)> pred) {
      auto next = start;
      if(next>=nrows())
        return false;
      while(!pred(*this, next))
      {
        next++;
        if(next>=nrows())
          return false;
      }
      return true;
    }
  };
  
  struct Output {
    std::vector<double> ts;
    std::vector<double> price;
    std::vector<double> qty;
    std::vector<const char*> op;
    
    void push_back(const OrderMessage &ord) {
      ts.push_back(ord.ts);
      price.push_back(ord.price);
      qty.push_back(ord.qty);
      op.push_back(format_op(ord));
    }
    List toR() const {
      List result;
      result.push_back(Function("as_datetime")(NumericVector(ts.begin(), ts.end())), "client_ts");
      result.push_back(NumericVector(price.begin(), price.end()), "price");
      result.push_back(NumericVector(qty.begin(), qty.end()), "qty");
      result.push_back(CharacterVector(op.begin(), op.end()), "op");
      return Function("as_tibble")(result);
    }
  };
  
  size_t i = optional<IntegerVector>(params, "index", 0)[0] - 1;

  Rcout << "level2_to_level1: params=\n";
  Rf_PrintValue(params);
  
  //auto qty_limits = optional<NumericVector>(params, "qty_limits", 0.0); // (best) bid, bid1, bid2, bid3 etc
  

  Book book;

  Input inp(data);
  Output outp;
  
  std::vector<Book> calculated;
  std::vector<Book> received;
  
  std::queue<OrderMessage> snapshots;
  std::queue<OrderMessage> updates;
  
  auto proceed = [&](const OrderMessage &ord) {
    int flags = book.update(ord);
    if(flags&Book::BID)
      outp.push_back(OrderMessage::from(book.bid(), ord.ts, OrderMessage::BID|OrderMessage::BEST|OrderMessage::UPDATE));
    if(flags&Book::ASK)
      outp.push_back(OrderMessage::from(book.ask(), ord.ts, OrderMessage::ASK|OrderMessage::BEST|OrderMessage::UPDATE));
  };
  
  auto proceed_all = [&](std::queue<OrderMessage> &queue) {
    while(!queue.empty()) {
      proceed(queue.front());
      queue.pop();
    }
  };
  
  int updates_in_row = 0;
  int max_updates_in_row = 1000;
  while(i<inp.nrows()) {
      OrderMessage ord(inp.price[i], inp.qty[i], inp.ts[i], parse_op(inp.op[i]));
      ord.server_id = inp.server_id[i];
      ord.client_id = inp.client_id[i];
      bool should_close_snapshot = false;
      if(ord.flags & OrderMessage::SNAPSHOT) {
        if(!snapshots.empty() && snapshots.back().client_id!=ord.client_id) {
          book.clear();
          proceed_all(snapshots);
          Rcout << "snapshot2 received "<< book.size();
          received.push_back(book);
        }
        if(snapshots.empty()) {
          calculated.push_back(book);
          Rcout << "snapshot calculated "<<book.size();
          book.clear();
        }
        snapshots.push(ord);
        updates_in_row = 0;
      }else if(ord.flags & OrderMessage::UPDATE) {
        if(!snapshots.empty()) { // update inside of the snapshot
          updates.push(ord);
          if(++updates_in_row>=max_updates_in_row || i==inp.nrows()-1) {
            while(updates.size()>max_updates_in_row) { // apply updates to prev snapshot
              proceed(updates.front());
              updates.pop();
            }
            book.clear();
            proceed_all(snapshots); // apply snapshot 
            Rcout << "snapshot received "<< book.size();
            received.push_back(book);
            proceed_all(updates); // these long sequence of updates should be applied to next snapshot
          }
        }else
          proceed(ord); // just regular update
      }
      i++;
  }
  List result;
  result.push_back(IntegerVector::create(i+1), "index");
  result.push_back(outp.toR(), "out");
  result.push_back(book.toR(), "snapshot");
  result.push_back(toR(calculated), "calculated");
  result.push_back(toR(received), "received");
  return result;
}


