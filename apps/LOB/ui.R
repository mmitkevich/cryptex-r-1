library("shinyWidgets")
fluidPage(
  titlePanel("Hello, LOB!"),
  fluidRow(
    column(4,
      wellPanel(
        sliderTextInput("since", "since", c("1m","1w","5d","3d","2d","1d","4H","1H","15M","5M","1M"), "4H"),
        sliderTextInput("duration", "duration", c("1m","1w","1d","4H","1H","15M","5M","1M", "0s"),"1m"),
        textInput("symbol","symbol",value="BTC:USD@BITMEX_GATEWAY"))),
    column(8,
      wellPanel(plotOutput("plot"),plotOutput("chart")))))
