library(cryptexr)
library(tidyverse)
library(gridExtra)
library(RClickhouse)
options(pillar.sigfig=7)
options(digits.secs=4)  

## server.R ##
tdb_connect(host="localhost", user="covexity", password="rmustdie")

quer <- function(from="taq", symbols=NULL, start_ts=NULL, stop_ts=NULL) {
  cat("quer(start=",as.character(start_ts),",stop=",as.character(stop_ts),"\n")
  tdb_query(from=from, symbols=symbols, start_ts = start_ts,
            stop_ts = stop_ts)
}

plot_taq <- function(symbols="BTC:USD@BITMEX_GATEWAY",...) {
  r<-quer(from="taq", symbols=symbols, ...)
  r %>% tdb_plot()
}
  

plot_order_book <- function(symbols="BTC:USD@BITMEX_GATEWAY",...) {
  r <- quer(from="order_book", symbols=symbols, ...) %>% 
        level2_to_level1(list(index=1))
  ds <- list(last=r$snapshot) 
  t <- paste0(max(r$out$client_ts) %>% as.character()," ",reduce(symbols,paste))
  plot_lob(ds, title=t)
}

ts_range <- function(input) {
  t <- lubridate::now()
  duration <- input$duration
  start_ts = t - lubridate::as.period(input$since)
  stop_ts = start_ts +  lubridate::as.period(duration)
  list(start_ts=start_ts, stop_ts=stop_ts)
} 

function(input, output) {
  output$chart <- renderPlot({
    ts <- ts_range(input)
    plot_taq(symbols=input$symbol, 
             start_ts=ts$start_ts, 
             stop_ts=ts$stop_ts)
  })
  output$plot <- renderPlot({
    ts <- ts_range(list(since="4h", duration="4h"))
    plot_order_book(symbols=input$symbol, 
       start_ts=ts$start_ts,
       stop_ts=ts$stop_ts)
  })
}